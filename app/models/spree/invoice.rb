require 'spree_invoices/invoice_pdf'

module Spree
  class Invoice < ActiveRecord::Base
    INVOICES_PATH = Rails.root.join 'invoices'

    after_create :create_pdf_file

    belongs_to :order
    belongs_to :parent_invoice, class_name: 'Spree::Invoice',
      foreign_key: 'parent_invoice_id'
    has_one :additional_invoice, class_name: 'Spree::Invoice'

    validates :order, presence: true

    default_scope { order created_at: :asc }

    def to_param
      "#{order.number}-#{id}"
    end

    def concept
      concept = if total < 0
                  "Abono factura anterior"
                else
                  "cargo adicional factura anterior"
                end
      concept << " (factura #{parent_invoice_id}), pedido #{order.number}"
    end

    def total_without_taxes
      total / (1 + vat_rate)
    end

    def create_pdf_file!
      create_pdf_file
    end
    private

    def create_pdf_file
      FileUtils.mkdir_p INVOICES_PATH
      filename = "#{self.order.number}-#{self.id}.pdf"
      pdf = SpreeInvoices::InvoicePdf.new(self)
      pdf.render_file Rails.root.join(INVOICES_PATH, filename)
      update_column :filename, filename
    end
  end
end

