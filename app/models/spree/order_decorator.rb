module Spree
  Order.class_eval do
    has_many :invoices, dependent: :destroy

    register_update_hook :conditional_create_first_invoice

    def needs_additional_invoice?
      self.payment_state == 'paid' && (
        (self.invoices.one? && self.total != self.invoices.last.total) ||
        (self.invoices.count > 1 && invoices_total != self.total)
      )
    end

    def create_additional_invoice
      last_invoice = self.invoices.last
      inv_total = self.total - invoices_total

      self.invoices.create(
        total: inv_total,
        parent_invoice: last_invoice,
        vat_rate: vat_rate_for_invoices,
        tax_total: inv_total - (inv_total / (1 + vat_rate_for_invoices))
      )
    end

    def conditional_create_first_invoice
      if needs_first_invoice?
        self.invoices.create(total: self.total,
                             vat_rate: vat_rate_for_invoices,
                             shipment_without_taxes: self.shipment_total / (1 + vat_rate_for_invoices),
                             tax_total: self.included_tax_total)
      end
    end

    private

    def needs_first_invoice?
      self.state == 'complete' and self.payment_state == 'paid' and
        self.invoices.empty?
    end

    def invoices_total
      self.invoices.sum(&:total)
    end

    def vat_rate_for_invoices
      @vat_rate_for_invoices ||= self.tax_zone.tax_rates.first.amount
    end
  end
end

