module Spree
  class InvoicesConfiguration < Preferences::Configuration

    preference :invoices_logo_path, :string, default: Spree::Config[:admin_interface_logo]
  end
end
