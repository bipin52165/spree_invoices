require 'csv'

module Spree
  module Admin
    ReportsController.class_eval do
      def initialize
        super
        ReportsController.add_available_report!(:invoiced_total)
        ReportsController.add_available_report!(:sales_total)
      end

      def invoiced_total
        params[:q] = {} unless params[:q]

        if params[:q][:created_at_gt].blank?
          params[:q][:created_at_gt] = Time.zone.now.beginning_of_month
        else
          params[:q][:created_at_gt] = Time.zone.parse(params[:q][:created_at_gt]).beginning_of_day rescue Time.zone.now.beginning_of_month
        end

        if params[:q] && !params[:q][:created_at_lt].blank?
          params[:q][:created_at_lt] = Time.zone.parse(params[:q][:created_at_lt]).end_of_day rescue ""
        end

        @search = Invoice.ransack(params[:q])
        @invoices = @search.result

        respond_to do |format|
          format.html
          format.csv do
            send_data generate_csv, filename: "facturacion_St_Honore.csv"
          end
        end
      end

      private

      def generate_csv
        CSV.generate do |csv|
          @invoices.each do |invoice|
            csv << [
              invoice.id,
              invoice.parent_invoice_id,
              invoice.order.number,
              invoice.order.email,
              invoice.total
            ]
          end
        end
      end
    end
  end
end

