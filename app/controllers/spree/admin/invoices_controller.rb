module Spree
  module Admin
    class InvoicesController < ResourceController
      respond_to :html
      before_action :load_order

      def index
        @invoices = @order.invoices
      end

      def create
        @invoice = @order.create_additional_invoice

        redirect_to action: :index
      end

      private

      def load_order
        @order = Spree::Order.find_by(number: params[:order_id])
      end
    end
  end
end

