module Spree
  class InvoicesController < Spree::StoreController
    respond_to :html

    def show
      @invoice = Spree::Invoice.find(params[:id].split('-')[1])
      authorize! :read, @invoice

      @order = @invoice.order

      respond_with @invoice do |format|
        format.html do
          send_file Rails.root.join(Spree::Invoice::INVOICES_PATH, @invoice.filename)
        end
      end
    end
  end
end

