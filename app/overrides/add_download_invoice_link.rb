Deface::Override.new(
  name: 'add_download_invoice_link',
  virtual_path: "spree/orders/show",
  insert_bottom: "#order[data-hook]",
  partial: "spree/orders/download_invoice",
  disabled: false
)

