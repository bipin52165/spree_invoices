Deface::Override.new(
  virtual_path: "spree/admin/shared/_order_submenu",
  name: "add_invoices_to_admin_orders_tabs",
  insert_bottom: "[data-hook='admin_order_tabs'], #admin_order_tabs[data-hook]",
  partial: "spree/admin/orders/invoices_tab",
  disabled: false
)