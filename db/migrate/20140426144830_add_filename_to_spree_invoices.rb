class AddFilenameToSpreeInvoices < ActiveRecord::Migration
  def change
    add_column :spree_invoices, :filename, :string
  end
end

