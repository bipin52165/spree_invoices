class CreateSpreeInvoices < ActiveRecord::Migration
  def change
    create_table :spree_invoices do |t|
      t.references :order
      t.references :parent_invoice
      t.decimal :total, precision: 10, scale: 2, default: 0.0
      t.decimal :additional_tax_total, precision: 10, scale: 2, default: 0.0
      t.decimal :included_tax_total, precision: 10, scale: 2, default: 0.0
      t.timestamps
    end

    add_index :spree_invoices, [:order_id]
    add_index :spree_invoices, [:parent_invoice_id]
  end
end
