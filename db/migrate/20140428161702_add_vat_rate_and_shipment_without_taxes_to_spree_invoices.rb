class AddVatRateAndShipmentWithoutTaxesToSpreeInvoices < ActiveRecord::Migration
  def change
    add_column :spree_invoices, :vat_rate, :decimal, precision: 10, scale: 2, default: 0.0
    add_column :spree_invoices, :shipment_without_taxes, :decimal, precision: 10, scale: 2, default: 0.0
    remove_column :spree_invoices, :additional_tax_total, :decimal, precision: 10, scale: 2, default: 0.0
    rename_column :spree_invoices, :included_tax_total, :tax_total
  end
end
