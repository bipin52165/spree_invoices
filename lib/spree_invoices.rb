require 'spree_core'
require 'spree_invoices/engine'

module Spree
  module Invoices
    def self.config(&block)
      yield(Spree::Invoices::Config)
    end
  end
end
