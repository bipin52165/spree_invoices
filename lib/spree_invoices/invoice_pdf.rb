require 'prawn'
require 'prawn/layout'

module SpreeInvoices
  class InvoicePdf < ::Prawn::Document

    def initialize(invoice)
      super()
      @invoice = invoice
      @order = @invoice.order

      print_header

      if !invoice.parent_invoice_id
        generate_original
      else
        generate_additional
      end

      print_footer
    end

    def print_header
      font "Helvetica"
      im = Rails.application.assets.find_asset(Spree::Invoices::Config[:invoices_logo_path])
      image im , :at => [0,720] , :scale => 0.50

      fill_color "000000"

      text "#{Spree.t(:invoice_number)}: #{@invoice.id}", :align => :right

      move_down 2
      text "#{Spree.t(:invoice_date)}: #{I18n.l @invoice.created_at.to_date}", :align => :right

      move_down 57
      font "Helvetica",  :size => 10

      bill_address = @order.bill_address

      data = ["#{bill_address.firstname} #{bill_address.lastname}"], [bill_address.address1]
      data << [bill_address.address2] unless bill_address.address2.blank?
      data << ["#{@order.bill_address.zipcode} #{@order.bill_address.city}#{(@order.bill_address.state ? ", " + @order.bill_address.state.name : "")} "]
      data << [bill_address.country.name]

      table data,
        position:            :left,
        border_width:        0.0,
        vertical_padding:    0,
        horizontal_padding:  6,
        vertical_padding:    2,
        font_size:           10,
        font_style:          :normal,
        column_widths:       { 0 => 270 }

        move_down 32
    end

    def generate_original
      @column_widths = { 0 => 95, 1 => 230, 2 => 70, 3 => 70, 4 => 75 }
      @align = { 0 => :center, 1 => :center, 2 => :center, 3 => :center, 4 => :center }

      font "Helvetica",  :size => 9,  :style => :bold

      # Line Items
      header =  [Prawn::Table::Cell.new( :text => Spree.t(:sku), :font_style => :bold),
                    Prawn::Table::Cell.new( :text => Spree.t(:item_description), :font_style => :bold ) ]
      header <<  Prawn::Table::Cell.new( :text => Spree.t(:price), :font_style => :bold )
      header <<  Prawn::Table::Cell.new( :text => Spree.t(:invoice_qty), :font_style => :bold, :align => 1 )
      header <<  Prawn::Table::Cell.new( :text => Spree.t(:subtotal), :font_style => :bold )

      table [header],
        position:           :left,
        border_width:       0.5,
        vertical_padding:   2,
        horizontal_padding: 6,
        vertical_padding:   4,
        font_size:          9,
        column_widths:      @column_widths ,
        align:              @align

      @align = { 0 => :left, 1 => :left, 2 => :right, 3 => :right, 4 => :right }

      font "Helvetica",  :size => 9
      content = []
      @order.line_items.each do |item|

        unit_price = Spree::Money.new(item.pre_tax_amount / item.quantity, currency: 'EUR')
        item_subtotal = Spree::Money.new(item.pre_tax_amount, currency: 'EUR')

        row = [ item.variant.product.sku, item.variant.product.name]
        row << unit_price.to_s unless @hide_prices
        row << item.quantity
        row << item_subtotal.to_s unless @hide_prices
        content << row
      end

      table content,
        position:           :left,
        border_width:       0.5,
        vertical_padding:   5,
        horizontal_padding: 6,
        font_size:          9,
        column_widths:      @column_widths,
        align:              @align

      bounding_box [0,cursor], :width => 540 do
        footer = []

        if @order.line_item_adjustments.exists?
          if @order.all_adjustments.promotion.eligible.exists?
            @order.all_adjustments.promotion.eligible.group_by(&:label).each do |label, adjustments|
              row = ["#{Spree.t(:promotion)}: #{label}"]
              row << Spree::Money.new(adjustments.sum(&:amount), currency: 'EUR').to_s
            end
            footer << row
          end
        end

        @order.shipments.each do |shipment|
#          shipment_price = Spree::Money.new(shipment.pre_tax_amount, currency: 'EUR').to_s
          shipment_price = Spree::Money.new(@invoice.shipment_without_taxes , currency: 'EUR').to_s
          row = ["#{Spree.t(:shipment)}:"]
          row << shipment_price.to_s
          footer << row
        end

        row = ["#{Spree.t :total_without_taxes}:"]
        row << Spree::Money.new(@invoice.total - @invoice.tax_total, currency: 'EUR').to_s
        footer << row

        row = ["IVA 21%:"]
        row << Spree::Money.new(@invoice.tax_total, currency: 'EUR').to_s
        footer << row

        column_widths = { 0 => 470, 1 => 70 }
        align = { 0 => :right, 1 => :right}

        table footer,
          position:           :center,
          border_width:       0,
          vertical_padding:   5,
          horizontal_padding: 6,
          column_widths:      column_widths,
          font_size:          9,
          align:              align

        font "Helvetica",  :size => 9,  :style => :bold
        table [["#{Spree.t :invoice_total}:", Spree::Money.new(@invoice.total, currency: 'EUR').to_s]],
          position:           :center,
          border_width:       0,
          vertical_padding:   5,
          horizontal_padding: 6,
          column_widths:      column_widths,
          font_size:          10,
          align:              align

        stroke do
          line_width 0.5
          line bounds.top_left, bounds.top_right
          line bounds.top_left, bounds.bottom_left
          line bounds.top_right, bounds.bottom_right
          line bounds.bottom_left, bounds.bottom_right
        end
      end
    end

    def generate_additional
      @column_widths = { 0 => 375, 1 => 165 }
      @align = { 0 => :center, 1 => :center }

      font "Helvetica",  :size => 9,  :style => :bold

      header =  [Prawn::Table::Cell.new( text: 'Concepto' ) ]
      header <<  Prawn::Table::Cell.new( :text => Spree.t(:subtotal), :font_style => :bold )

      table [header],
        position:           :left,
        border_width:       0.5,
        vertical_padding:   2,
        horizontal_padding: 6,
        vertical_padding:   4,
        font_size:          9,
        column_widths:      @column_widths ,
        align:              @align

      font "Helvetica",  :size => 9
      content = []
      row = [@invoice.concept]
      row << Spree::Money.new( @invoice.total_without_taxes, currency: 'EUR').to_s
      content << row

      @align = { 0 => :left, 1 => :center }

      table content,
        position:           :left,
        border_width:       0.5,
        vertical_padding:   5,
        horizontal_padding: 6,
        font_size:          9,
        column_widths:      @column_widths,
        align:              @align

      @align = { 0 => :right, 1 => :center }

      bounding_box [0,cursor], :width => 540 do
        footer = []

        row = ["#{Spree.t :total_without_taxes}:"]
        row << Spree::Money.new(@invoice.total_without_taxes, currency: 'EUR').to_s

        footer << row

        row = ["IVA 21%:"]
        row << Spree::Money.new(@invoice.tax_total, currency: 'EUR').to_s

        footer << row

        table footer,
          position:           :center,
          border_width:       0,
          vertical_padding:   5,
          horizontal_padding: 6,
          column_widths:      @column_widths,
          font_size:          9,
          align:              @align

        font "Helvetica",  :size => 9,  :style => :bold
        table [["#{Spree.t :invoice_total}:", Spree::Money.new(@invoice.total, currency: 'EUR').to_s]],
          position:           :center,
          border_width:       0,
          vertical_padding:   5,
          horizontal_padding: 6,
          column_widths:      @column_widths,
          font_size:          10,
          align:              @align

        stroke do
          line_width 0.5
          line bounds.top_left, bounds.top_right
          line bounds.top_left, bounds.bottom_left
          line bounds.top_right, bounds.bottom_right
          line bounds.bottom_left, bounds.bottom_right
        end
      end
    end

    def print_footer
      footer_message = Spree.t(:invoice_issued_by) + ": " +
        [BANK_TRANSFER_BENEFICIARY, Spree.t(:invoice_from_address),
         Spree.t(:invoice_from_zipcode), Spree.t(:invoice_company_tax_code)].join(", ")
      repeat :all do
        font "Helvetica", :size => 9
        text_box footer_message, :at => [(margin_box.left),
          margin_box.bottom + 20], :size => 8 , :position => :left,
          align: :center
      end
    end
  end
end

