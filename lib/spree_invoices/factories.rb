FactoryGirl.define do
  factory :invoice, class: Spree::Invoice do
  	order

  	factory :amending_invoice do
  	  association :parent_invoice, factory: :invoice
  	end
  end
end
