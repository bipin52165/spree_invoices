require 'spec_helper'

describe Spree::Invoice do
  let(:invoice) { create :invoice }

  it { invoice.should be_valid }
  it { invoice.respond_to?(:order).should be_true }
  it { invoice.respond_to?(:parent_invoice).should be_true }
  it { invoice.respond_to?(:amending_invoice).should be_true }

  it "without order is not valid" do
  	expect( build :invoice, order: nil).not_to be_valid
  end
end
