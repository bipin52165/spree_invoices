require 'spec_helper'

describe Spree::Order do
  let(:order) { create :order }

  it { order.respond_to?(:invoices).should be_true }

  it "calls #create_invoice during update" do
    order.should_receive(:conditional_create_first_invoice)
    order.update!
  end

  context "creating invoices" do
    it "creates a first invoice when complete and paid" do
      order.stub payment_state: 'paid'
      order.stub state: 'complete'
      order.update!
      expect( order.invoices.count ).to eq 1
    end
  end
end

