Spree::Core::Engine.routes.draw do
  namespace :admin do
    resources :orders do
      resources :invoices, only: [:index, :create]
    end

    resources :reports, only: [:index, :show] do
      collection do
        get :invoiced_total
        post :invoiced_total
      end
    end
  end

  resources :invoices, only: [:show]
end

